package com.syscom.oms.controller;

import com.syscom.oms.entity.Product;
import com.syscom.oms.repository.ProductRepository;
//import org.restlet.resource.ClientResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//import javax.ws.rs.client.Client;
//import javax.ws.rs.client.ClientBuilder;
//import javax.ws.rs.client.Invocation;
//import javax.ws.rs.client.WebTarget;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;


    @GetMapping("/allProducts")
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @PostMapping("/addProduct")
    public Product addProduct(@RequestBody Product product) {
        return productRepository.save(product);
    }

//    @GetMapping("/getProductFromGCP")
//    public List<Product> getFromGCP() {
//
//
//
//        Client client = ClientBuilder.newClient();
//
//        WebTarget resource = client.target("https://product-service-dot-oms-system-202009.df.r.appspot.com/allProducts");
//
//        Invocation.Builder request = resource.request();
//        request.accept(MediaType.APPLICATION_JSON);
//
//        Response response = request.get();
//
//        if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
//            System.out.println("Success! " + response.getStatus());
//            System.out.println(response.getEntity());
//        } else {
//            System.out.println("ERROR! " + response.getStatus());
//            System.out.println(response.getEntity());
//        }
//
//        return productRepository.findAll();
//    }
//
//    @GetMapping("/getProductFromGCPRestlet")
//    public List<Product> getRestLet(){
//        ClientResource resource = new ClientResource("https://product-service-dot-oms-system-202009.df.r.appspot.com/allProducts");
//
//        Response response = resource.post(form.getWebRepresentation());
//
//        if (response.getStatus().isSuccess()) {
//            System.out.println("Success! " + response.getStatus());
//            System.out.println(response.getEntity().getText());
//        } else {
//            System.out.println("ERROR! " + response.getStatus());
//            System.out.println(response.getEntity().getText());
//        }
//        return productRepository.findAll();
//    }

}
